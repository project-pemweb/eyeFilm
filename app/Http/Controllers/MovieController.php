<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Genre;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index()
    {
        return view('movies', [
            "title" => "Daftar Film",
            "movies" => Movie::all()
        ]);
    }

    public function show() {
        return view('movie', [
            "title" => "Film",
            "movies" => Movie::all()
        ]);
    }
}
