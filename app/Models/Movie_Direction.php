<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie_Direction extends Model
{
    use HasFactory;

    protected $fillable = ['director_id', 'movie_id'];
}
