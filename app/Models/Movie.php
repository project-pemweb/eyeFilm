<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'movie__genres');
    }

    public function directors()
    {
        return $this->belongsToMany(Director::class, 'movie__directions');
    }

    public function actors()
    {
        return $this->belongsToMany(Actor::class, 'movie__casts');
    }
}
