<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>eyeFilm | {{ $title }}</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="text-neutral-800">
    <!-- component -->
    @include('components.navbar')
    <div class="w-full">
        <div class="container max-w-screen-2xl mt-4 mx-auto px-8 md:px-10 lg:px-12">
            @yield('container')
        </div>
    </div>
</body>

</html>
