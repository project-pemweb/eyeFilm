@extends('layouts.main')

@section('container')
    <h1 class="font-bold text-3xl">{{ $title }}</h1>
    <div class="movie-cards grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-8">
        @foreach ($movies as $movie)
            @include('components.movie-card')
        @endforeach
    </div>
@endsection