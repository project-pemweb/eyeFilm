<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\MovieController;
use Illuminate\Support\Facades\Route;
use App\Models\Post;
use App\Models\Movie;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('home', [
        "title" => "Home"
    ]);
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('auth/login', [
        "title" => "login"
    ]);
});

Route::get('/movies/{slug}', [MovieController::class, 'show']);
Route::get('/movies', [MovieController::class, 'index']);
